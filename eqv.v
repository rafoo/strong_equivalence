Set Universe Polymorphism.

Notation "'sigma' x .. y , p" := (sigT (fun x => .. (sigT (fun y => p%type)) ..))
  (at level 200, x binder, right associativity,
   format "'[' 'sigma'  '/  ' x  ..  y ,  '/  ' p ']'")
  : type_scope.

Definition comp {A B C : Type} (f : A -> B) (g : B -> C) := fun x => g (f x).
Definition id (A : Type) := fun x : A => x.

Inductive eq {A : Type} (a : A) : A -> Type := eq_refl : eq a a.

Notation "a == b" := (eq a b) (at level 70).

Lemma eq_sym {A} {x y : A} : x == y -> y == x.
Proof.
  intros [].
  reflexivity.
Defined.

Lemma eq_trans {A} {x y z : A} : x == y -> y == z -> x == z.
Proof.
  intros [] [].
  reflexivity.
Defined.

Lemma eq_trans_assoc {A} {w x y z : A} (p1 : w == x) (p2 : x == y) (p3 : y == z) :
  eq_trans (eq_trans p1 p2) p3 == eq_trans p1 (eq_trans p2 p3).
Proof.
  destruct p1, p2, p3.
  reflexivity.
Defined.

Lemma eq_trans_sym {A} {x y : A} (p1 : eq x y) :
  eq_trans p1 (eq_sym p1) == eq_refl x.
Proof.
  destruct p1.
  reflexivity.
Defined.

Lemma eq_trans_refl {A} {x y : A} (p : x == y) : eq_trans p (eq_refl y) == p.
Proof.
  destruct p; reflexivity.
Defined.

Lemma eq_trans_sym_eq {A} {x y z : A} (p : x == y) (q : x == z) (r : y == z) :
  q == eq_trans p r -> eq_trans (eq_sym p) q == r.
Proof.
  destruct r; simpl; destruct q; simpl.
  intro H.
  assert (X : eq_refl x == p).
  - apply (@eq_trans (x == x) (eq_refl x) (eq_trans p (eq_refl x)) p).
    + apply H.
    + apply eq_trans_refl.
  - destruct X.
    reflexivity.
Defined.

Definition f_equal {A B} (f : A -> B) {x y : A} (p : x == y) : f x == f y.
Proof.
  destruct p.
  reflexivity.
Defined.

Lemma f_equal_id {A} {x y : A} (p : x == y) : f_equal (id A) p == p.
Proof.
  destruct p.
  reflexivity.
Defined.

Lemma f_equal_comp {A B C} (f : A -> B) (g : B -> C) x y (p : x == y) :
  f_equal g (f_equal f p) == f_equal (comp f g) p.
Proof.
  destruct p.
  reflexivity.
Defined.

Lemma natural_transf {A B} {f g : A -> B} (H : forall x, f x == g x) x y (p : x == y) :
  eq_trans (H x) (f_equal g p) == eq_trans (f_equal f p) (H y).
Proof.
  destruct p.
  simpl.
  destruct (H x).
  reflexivity.
Defined.

Definition isContr (A : Type) := sigma x : A, forall y : A, x == y.
Definition isProp A := forall a b : A, a == b.

Definition funext_statement :=
  forall A B (f g : forall x : A, B x), (forall x : A, f x == g x) -> f == g.

Definition pinv {A B} (f : A -> B) (g : B -> A) :=
  forall x : B, f (g x) == x.

Definition qinv {A B} (f : A -> B) := sigma g : B -> A, pinv f g * pinv g f.

Definition fiber {A B} (f : A -> B) (y : B) := sigma x : A, f x == y.

(* We choose this definition for equivalence (contractible function)
because a rather weak form of functional extensionality is enough to
prove that it is a mere proposition. *)
Definition isEqv {A B} (f : A -> B) :=
  forall y : B, isContr (fiber f y).

Definition eqv A B := sigma f : A -> B, isEqv f.

Notation "A ≃ B" := (eqv A B) (at level 70).

Definition eqv_fun {A B} (e : A ≃ B) : A -> B :=
  let (f, _) := e in f.

Coercion eqv_fun : eqv >-> Funclass.

(* This other possible definition of equivalence is used to show qinv f -> isEqv f *)
Definition half_adjoint {A B} f :=
  sigma (g : B -> A) (eta : pinv g f) (eps : pinv f g),
    forall x, f_equal f (eta x) == eps (f x).

Lemma qinv_half_adjoint {A B} (f : A -> B) : qinv f -> half_adjoint f.
  intros (g, (eps, eta)).
  exists g.
  exists eta.
  exists (fun b => eq_trans (eq_sym (eps (f(g(b))))) (eq_trans (f_equal f (eta (g b))) (eps b))).
  intro a.
  symmetry.
  assert (H : eta (g (f a)) == f_equal g (f_equal f (eta a))).
  - assert (forall p, eq_trans (eta (g (f a))) p == eq_trans (f_equal g (f_equal f p)) (eta a)) as X.
    + intro p.
      destruct p.
      simpl.
      destruct (eta (g (f a))).
      reflexivity.
    + specialize (X (eta a)).
      apply (f_equal (fun e => eq_trans e (eq_sym (eta a)))) in X.
      rewrite eq_trans_assoc in X.
      rewrite eq_trans_sym in X.
      simpl in X.
      rewrite eq_trans_assoc in X.
      rewrite eq_trans_sym in X.
      apply (eq_trans (eq_sym (eq_trans_refl (eta (g (f a)))))).
      apply (eq_trans X).
      apply eq_trans_refl.
  - rewrite H.
    apply eq_trans_sym_eq.
    rewrite f_equal_comp.
    symmetry.
    refine (eq_trans _ (natural_transf eps _ _ (f_equal f (eta a)))).
    rewrite f_equal_id.
    reflexivity.
Defined.

Lemma half_adjoint_isEqv {A B} (f : A -> B) :
  half_adjoint f -> isEqv f.
Proof.
  intros (g, (eta, (eps, tau))) y.
  exists (existT _ (g y) (eps y)).
  intros (x, Hx).
  destruct Hx.
  rewrite <- tau.
  destruct (eta x).
  reflexivity.
Defined.

Lemma qinv_isEqv {A B} {f : A -> B} : qinv f -> isEqv f.
Proof.
  intro H.
  apply half_adjoint_isEqv.
  apply qinv_half_adjoint.
  exact H.
Defined.

(* Going in the other direction (from isEqv to qinv) is easy: *)
Definition isEqv_inv {A B} (f : A -> B) :
  isEqv f -> B -> A.
Proof.
  intros Hf y.
  destruct (Hf y) as ((x, _), _).
  exact x.
Defined.

Lemma isEqv_inv_eta {A B} (f : A -> B) (Hf : isEqv f) :
  pinv (isEqv_inv f Hf) f.
Proof.
  intro x.
  unfold isEqv_inv.
  destruct (Hf (f x)) as ((x', Hx'), H').
  specialize (H' (existT (fun x' => f x' == f x) x (eq_refl (f x)))).
  apply (f_equal (@projT1 A (fun x' => f x' == f x))) in H'.
  exact H'.
Defined.

Lemma isEqv_inv_eps {A B} (f : A -> B) (Hf : isEqv f) :
  pinv f (isEqv_inv f Hf).
Proof.
  intro y.
  unfold isEqv_inv.
  destruct (Hf y) as ((x', Hx'), H').
  assumption.
Defined.

Lemma isEqv_qinv {A B} (f : A -> B) : isEqv f -> qinv f.
Proof.
  intro Hf.
  exists (isEqv_inv _ Hf).
  split.
  - apply isEqv_inv_eps.
  - apply isEqv_inv_eta.
Defined.

Section with_funext.
  Hypothesis funext : funext_statement.

  Lemma isProp_isContr A : isProp (isContr A).
  Proof.
    intros a b.
    destruct a as (a, Ha).
    destruct b as (b, Hb).
    transitivity
      (existT (fun x : A => forall y : A, x == y) a
              (fun y => eq_trans (Ha b) (Hb y))).
    - apply f_equal.
      apply funext.
      intro c.
      destruct (Hb c).
      apply eq_sym.
      apply eq_trans_refl.
    - destruct (Ha b).
      apply f_equal.
      apply funext.
      intro b.
      destruct (Hb b).
      reflexivity.
  Defined.

  Lemma isProp_isEqv {A B} (f : A -> B) :
    isProp (isEqv f).
  Proof.
    intros H1 H2.
    apply funext.
    intro x.
    apply isProp_isContr.
  Defined.
End with_funext.

Lemma isEqv_id A : isEqv (id A).
Proof.
  apply qinv_isEqv.
  exists (id A).
  split; exact (fun x => eq_refl x).
Defined.

Definition eqv_refl A : A ≃ A := existT _ (id A) (isEqv_id A).

Lemma id_to_eqv {A B} : A == B -> A ≃ B.
Proof.
  intros [].
  apply eqv_refl.
Defined.

Lemma isEqv_intro {A B} {f : A -> B} g : pinv f g -> pinv g f -> isEqv f.
Proof.
  intros eps eta.
  apply qinv_isEqv.
  exists g.
  split; assumption.
Defined.

Lemma inv_isEqv {A B} {f : A -> B} (H : isEqv f) : isEqv (isEqv_inv f H).
Proof.
  apply (isEqv_intro f).
  - apply isEqv_inv_eta.
  - apply isEqv_inv_eps.
Defined.

Lemma eqv_sym {A B} : A ≃ B -> B ≃ A.
Proof.
  intros (f, H).
  exists (isEqv_inv f H).
  apply inv_isEqv.
Defined.

Lemma eqv_sym_eps {A B} (f : A ≃ B) : pinv f (eqv_sym f).
Proof.
  destruct f as (f, Hf).
  simpl.
  apply isEqv_inv_eps.
Defined.

Lemma eqv_sym_eta {A B} (f : A ≃ B) : pinv (eqv_sym f) f.
Proof.
  destruct f as (f, Hf).
  simpl.
  apply isEqv_inv_eta.
Defined.

Lemma eqv_trans {A B C} : A ≃ B -> B ≃ C -> A ≃ C.
Proof.
  intros (f, Hf) (g, Hg).
  exists (comp f g).
  apply (isEqv_intro (comp (isEqv_inv g Hg) (isEqv_inv f Hf))).
  - intro c.
    unfold comp.
    rewrite isEqv_inv_eps.
    apply isEqv_inv_eps.
  - intro a.
    unfold comp.
    rewrite isEqv_inv_eta.
    apply isEqv_inv_eta.
Defined.

Lemma isContr_retract {A B} (f : A -> B) (g : B -> A) :
  pinv f g -> isContr A -> isContr B.
Proof.
  intros H (a, Ha).
  exists (f a).
  intro y.
  rewrite <- H.
  apply f_equal.
  apply Ha.
Defined.

Lemma happly {A B} (f g : forall x : A, B x) : f == g -> forall x, f x == g x.
Proof.
  intros [].
  reflexivity.
Defined.

Lemma between_contr_isEqv {A B} (f : A -> B) :
  isContr A -> isContr B -> isEqv f.
Proof.
  intros (a, HA) (b, HB).
  apply qinv_isEqv.
  exists (fun y => a).
  split.
  - intro y.
    transitivity b.
    + symmetry.
      apply HB.
    + apply HB.
  - intro x.
    apply HA.
Defined.

Lemma isContr_eqv {A B} (f : A ≃ B) : isContr A -> isContr B.
Proof.
  intros (a, HA).
  exists (f a).
  intro y.
  specialize (HA (eqv_sym f y)).
  rewrite HA.
  apply eqv_sym_eps.
Defined.

Definition total {A P Q} (f : forall x : A, P x -> Q x) :
  (sigma x : A, P x) -> (sigma x : A, Q x) :=
  (fun w => existT Q (projT1 w) (f (projT1 w) (projT2 w))).

Definition transport {A} (P : A -> Type) {x y : A} (p : x == y) : P x -> P y.
Proof.
  destruct p; auto.
Defined.

Lemma fiber_total_fun {A P Q} (f : forall x : A, P x -> Q x) (w : sigma x, Q x) :
  (fiber (total f) w) -> (fiber (f (projT1 w)) (projT2 w)).
Proof.
  intros ((a, p), H).
  unfold total in H.
  simpl in H.
  unfold fiber.
  exists (transport P (f_equal (@projT1 A Q) H) p).
  destruct H.
  reflexivity.
Defined.

Lemma fiber_total_inv {A P Q} (f : forall x : A, P x -> Q x) (w : sigma x, Q x) :
  (fiber (f (projT1 w)) (projT2 w)) -> (fiber (total f) w).
Proof.
  destruct w as (x, v).
  intros (p, H).
  unfold total, fiber.
  exists (existT P x p).
  simpl.
  apply f_equal.
  exact H.
Defined.

Lemma fiber_total {A P Q} (f : forall x : A, P x -> Q x) w :
  eqv (fiber (total f) w) (fiber (f (projT1 w)) (projT2 w)).
Proof.
  exists (fiber_total_fun f w).
  apply (isEqv_intro (fiber_total_inv f w)).
  - destruct w as (x, v).
    simpl.
    intros (p, H).
    simpl.
    destruct H.
    reflexivity.
  - intros ((a, p), H).
    simpl.
    destruct H.
    reflexivity.
Defined.

Definition fiberwise_eqv {A P Q} (f : forall x : A, P x -> Q x) :=
  forall x, isEqv (f x).

Lemma fiberwise_eqv_total {A P Q} (f : forall x : A, P x -> Q x) :
  isEqv (total f) -> fiberwise_eqv f.
Proof.
  unfold isEqv.
  intros H x.
  intro q.
  apply (isContr_eqv (fiber_total f (existT Q x q))).
  apply H.
Defined.

Section with_weak_funext.

  (* The proof of weak-funext -> funext from the HoTT book. *)

  Hypothesis weak_funext : forall A (P : A -> Type),
    (forall x, isContr (P x)) -> isContr (forall x, P x).

  Definition codom_happly1 {A B} (f : forall x : A, B x) :=
    sigma (g : forall x, B x), forall x, f x == g x.

  Lemma codom_happly1_to_fun {A B} (f : forall x : A, B x) :
    codom_happly1 f -> forall x, sigma u : B x, f x == u.
  Proof.
    intros (g, H) x.
    exists (g x).
    apply H.
  Defined.

  Lemma fun_to_codom_happly1 {A B} (f : forall x : A, B x) :
    (forall x, sigma u : B x, f x == u) -> codom_happly1 f.
  Proof.
    intros H.
    exists (fun x => projT1 (H x)).
    intro x.
    destruct (H x) as (u, Hu).
    exact Hu.
  Defined.

  Lemma isEqv_happly {A B} (f g : forall x : A, B x) : isEqv (happly f g).
  Proof.
    generalize g; clear g.
    apply fiberwise_eqv_total.
    apply between_contr_isEqv.
    - exists (existT _ f (eq_refl f)).
      intros (g, H).
      destruct H.
      reflexivity.
    - apply (isContr_retract (fun_to_codom_happly1 f) (codom_happly1_to_fun f)).
      + intros (g, H).
        reflexivity.
      + apply weak_funext.
        intro x.
        exists (existT _ (f x) (eq_refl _)).
        intros (y, Hy).
        destruct Hy.
        reflexivity.
  Qed.

  Lemma happly_eqv {A B} (f g : forall x : A, B x) : (f == g) ≃ (forall x, f x == g x).
  Proof.
    exists (happly f g).
    apply isEqv_happly.
  Defined.

  Theorem funext : funext_statement.
  Proof.
    intros A B f g.
    exact (eqv_sym (happly_eqv f g)).
  Defined.

  Lemma funext_id {A B} {f : forall x : A, B x} :
    funext A B f f (fun x => eq_refl (f x)) == eq_refl f.
  Proof.
    change (funext A B f f (happly_eqv f f (eq_refl f)) == eq_refl f).
    apply (eqv_sym_eta (happly_eqv f f)).
  Defined.

End with_weak_funext.

Lemma isProp_eqv {A B} : isProp A -> A ≃ B -> isProp B.
Proof.
  intros HA HAB b1 b2.
  destruct HAB as (f, Hf).
  rewrite <- (isEqv_inv_eps _ Hf b1).
  rewrite <- (isEqv_inv_eps _ Hf b2).
  apply f_equal.
  apply HA.
Defined.

Lemma isProp_to_isContr {A : Type} (a : A) : isProp A -> isContr A.
Proof.
  intro H.
  exists a.
  apply H.
Defined.

Lemma eqv_prod {A A' B B'} : A ≃ B -> A' ≃ B' -> (A * A') ≃ (B * B').
Proof.
  intros (f, H) (f', H').
  exists (fun c : A * A' => let (a, a') := c in (f a, f' a')).
  apply (isEqv_intro (fun c : B * B' => let (b, b') := c in (isEqv_inv _ H b, isEqv_inv _ H' b'))).
  - intros (b, b').
    rewrite isEqv_inv_eps.
    rewrite isEqv_inv_eps.
    reflexivity.
  - intros (a, a').
    rewrite isEqv_inv_eta.
    rewrite isEqv_inv_eta.
    reflexivity.
Defined.

Lemma isContr_unit {A} : isContr A -> A ≃ unit.
Proof.
  intros (x, H).
  exists (fun _ => tt).
  apply (isEqv_intro (fun _ => x)).
  - intros [].
    reflexivity.
  - intro y.
    apply H.
Defined.

Lemma eqv_prod_unit {A} : (A * unit) ≃ A.
Proof.
  exists (fun au : A * unit => let (a, _) := au in a).
  apply (isEqv_intro (fun a => (a, tt))).
  - intro.
    reflexivity.
  - intros (a, []).
    reflexivity.
Defined.

Lemma isEqv_homotopy {A B} (f g : A -> B) :
  isEqv f -> (forall x, f x == g x) -> isEqv g.
Proof.
  intros Hf Hfg.
  apply isEqv_qinv in Hf.
  destruct Hf as (h, (Hhf, Hfh)).
  apply (isEqv_intro h).
  - intro y.
    rewrite <- Hfg.
    apply Hhf.
  - intro x.
    rewrite <- Hfg.
    apply Hfh.
Defined.
