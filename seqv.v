Require Import HoTT.

Require Import Types.Universe.

Set Printing Universes.

Require FunextVarieties.
Require Types.Equiv.

Import Core.

Axiom Funext_type_implies_Funext : Funext_type -> Funext.


(* This file studies the properties of the following inductive type: *)
Inductive isStrongEqv@{i} {A : Type@{i}} :
  forall {B : Type@{i}}, (A -> B) -> Type@{i + 1} :=
| isStrongEqv_id : isStrongEqv idmap.

(* The main result is Univalence <-> (forall f, eqv (isEqv f) (isStrongEqv f))
                                 <-> (forall f, qinv f -> isStrongEqv f) *)

(* As a corrolary, assuming univalence, this inductive type is a good definition
   of isEqv since it has the following properties:
   1) forall A B (f : A -> B), qinv f -> isStrongEqv f
   2) forall A B (f : A -> B), isStrongEqv f -> qinv f
   3) forall A B (f : A -> B), isProp (isStrongEqv f)
   4) forall A B, qinv (eq_to_seqv A B) *)

(* However, it differs from other definitions of equivalence by the
   axioms needed to prove each of these facts. Usually, 1) and 2) are
   axiom-free, 3) requires functional extensionality and 4) is
   logically equivalent to univalence. Here 2) and 4) are axiom-free, 1) is
   logically equivalent to univalence and 3) is a consequence of
   univalence. Hence when we can prove that a given function is a
   strong equivalence without using any axiom, we can then compute
   with it through 4). A typical example are functions defined by
   transport. *)


Record seqv@{i} (A B : Type@{i}) : Type@{i+1} :=
  { f : A -> B;
    Hf : isStrongEqv@{i} f}.
Notation "A ≃ B" := (seqv A B) (at level 70).

Definition seqv_fun@{i j} (A B : Type@{i}) (e : A ≃ B) : A -> B := f A B e.
Coercion seqv_fun : seqv >-> Funclass.

(* Strong equivalence is reflexive *)
Lemma seqv_refl@{i} (A : Type@{i}) : A ≃ A.
Proof.
  eexists.
  constructor.
Defined.

Remark seqv_sym@{i} {A B : Type@{i}}: A ≃ B -> B ≃ A.
Proof.
  intros (f, Hf).
  destruct Hf.
  apply seqv_refl.
Defined.

Remark seq_sym_sym@{i j} {A B : Type@{i}} (e : A ≃ B) :
  paths@{j} (seqv_sym@{i} (seqv_sym@{i} e)) e.
Proof.
  destruct e as (f, Hf).
  destruct Hf.
  reflexivity.
Defined.

(* A strong equivalence is an equivalence *)
Lemma seqv_inv_fun@{i} {A B : Type@{i}} (f : A ≃ B) : B -> A.
Proof.
  destruct f as (f, Hf).
  destruct Hf.
  apply idmap.
Defined.

Lemma is_seqv_to_eqv@{i} {A B : Type@{i}} {f : A -> B} :
  isStrongEqv f -> IsEquiv f.
Proof.
  intros [].
  apply isequiv_idmap.
Defined.

Lemma seqv_to_eqv@{i} {A B : Type@{i}} : A ≃ B -> A <~> B.
Proof.
  intros (f, Hf).
  exists f.
  apply is_seqv_to_eqv.
  exact Hf.
Defined.

(* Strong equivalence is equivalent to equality *)
Lemma seqv_to_eq@{i j} {A B : Type@{i}} : A ≃ B -> paths@{j} A B.
Proof.
  intro f.
  destruct f as (f, Hf).
  destruct Hf.
  reflexivity.
Defined.

Lemma eq_to_seqv@{i j} {A B : Type@{i}} : paths@{j} A B -> A ≃ B.
Proof.
  intros [].
  apply seqv_refl.
Defined.

Lemma seqv_eqv_eq@{i j} {A B : Type@{i}} : (A ≃ B) <~> (paths@{j} A B).
Proof.
  exists seqv_to_eq.
  apply (isequiv_adjointify _ eq_to_seqv).
  - intros [].
    reflexivity.
  - intros (f, Hf).
    destruct Hf.
    reflexivity.
Defined.

Remark transport_seqv A (P : A -> Type) x y (e : x = y) :
  isStrongEqv (transport P e).
Proof.
  destruct e.
  constructor.
Qed.

Remark id_to_eqv_seqv A B (e : A = B) : isStrongEqv (equiv_path _ _ e).
Proof.
  destruct e.
  constructor.
Qed.

(*
   We finally show that the following statements are equivalent:
   1. univalence : equiv_path is an equivalence
   2. funext + (A <~> B) <~> (A = B)
   3. eqv_induction (without beta)
   4. isEqv f -> isStrongEqv f
   5. eqv_induction (with beta)
   6. isEqv f <~> isStrongEqv f
 *)

(*
   1->2 is folklore
 *)

Section from_weak_univ_and_funext_to_eqv_induction.

  Hypothesis funext :
    forall A B (f g : A -> B), (forall x : A, f x = g x) -> f = g.
  Hypothesis weak_univalence : forall A B, (A <~> B) <~> (A = B).
  Hypothesis weak_univalence_bis : forall A B, (A <~> B) <~> (A = B).

  Polymorphic Lemma eqv_impl_eq : forall A B, A <~> B -> A = B.
  Proof.
    apply weak_univalence_bis.
  Qed.

  Lemma eqv_is_eq : forall A, Equiv A = paths A.
  Proof.
    intro A.
    apply funext.
    intro B.
    apply eqv_impl_eq.
    apply (weak_univalence A B).
  Qed.

  (* From this equality, we can transport the induction principle for
     equality to equivalence. More precisely, we transfer the
     following variant of "based path induction" that avoids
     mentionning reflexivity. *)

  Definition has_eqind (eq : Type -> Type) :=
    forall (P : (forall B, eq B -> Type)) B e B' e', P B e -> P B' e'.

  (* (eq A) satisfies has_eqind *)
  Lemma has_eqind_eq A : has_eqind (paths A).
  Proof.
    intros P B e B' e'.
    destruct e.
    destruct e'.
    apply idmap.
  Qed.

  (* Hence (eqv A) also satisfies it. *)
  Lemma has_eqind_eqv A : has_eqind (Equiv A).
  Proof.
    rewrite eqv_is_eq.
    apply has_eqind_eq.
  Qed.

  Definition eqv_induction {A} (P : forall B, A <~> B -> Type) B e :
    P A (equiv_idmap A) -> P B e :=
    has_eqind_eqv A P A (equiv_idmap A) B e.

End from_weak_univ_and_funext_to_eqv_induction.

Section from_eqv_induction_and_IsEquiv_to_isStrongEqv.

  Variable eqv_induction : forall A (P : forall B, A <~> B -> Type) B e,
    P A (equiv_idmap A) -> P B e.

  Theorem iseqv_to_seqv {A B} (f : A -> B) : IsEquiv f -> isStrongEqv f.
  Proof.
    intro Hf.
    apply (eqv_induction A (fun B f => isStrongEqv f) B (Build_Equiv _ _ f Hf)).
    apply isStrongEqv_id.
  Defined.

End from_eqv_induction_and_IsEquiv_to_isStrongEqv.

Section from_eqvtoseqv_to_eqv_induction.

  Hypothesis iseqv_to_seqv : forall A B (f : A -> B), IsEquiv f -> isStrongEqv f.

  (* We first derive funext. This proof is taken from the HoTT book *)
  Section contractible_family.

    Theorem seq_exponential : forall {A B} (s : A ≃ B) C, (C -> A) ≃ (C -> B).
    Proof.
      intros A B (s, H) C.
      exists (fun h => s o h).
      destruct H.
      apply isStrongEqv_id.
    Defined.

    Variable A : Type.
    Variable P : A -> Type.
    Hypothesis C : forall x : A, Contr (P x).

    Lemma alpha : (A -> {x : A & P x}) ≃ (A -> A).
    Proof.
      apply seq_exponential.
      exists (@projT1 A P).
      apply iseqv_to_seqv.
      apply isequiv_pr1_contr.
    Defined.

    Definition phi (f : forall x : A, P x) : hfiber alpha (fun x : A => x) :=
      existT _ (fun x => existT P x (f x)) (idpath _).

    Definition psi (gp : hfiber alpha (fun x : A => x)) (x : A) : P x.
      destruct gp as (g, p).
      unfold alpha in p.
      simpl in p.
      apply (transport (fun f => P (f x)) p).
      exact (projT2 (g x)).
    Defined.

    Lemma weak_extensionality : Contr (forall x, P x).
    Proof.
      refine (contr_retract psi phi _).
      - destruct alpha as (alpha, Ha).
        simpl.
        apply is_seqv_to_eqv in Ha.
        apply contr_map_isequiv.
        apply Ha.
      - intro x.
        reflexivity.
    Qed.
  End contractible_family.

  Definition funext :
    Funext_type :=
    FunextVarieties.WeakFunext_implies_Funext weak_extensionality.

  Lemma eqv_to_seqv {A B} : A <~> B -> A ≃ B.
  Proof.
    intros (f, Hf).
    exists f.
    apply iseqv_to_seqv.
    exact Hf.
  Defined.

  Lemma seqv_ind' A (P : forall B, A ≃ B -> Type) B e B' e' :
    P B e -> P B' e'.
  Proof.
    destruct e' as (f', H').
    destruct H'.
    destruct e as (f, H).
    destruct H.
    apply idmap.
  Defined.

  Lemma seqv_ind_id A P B e : seqv_ind' A P B e B e = idmap.
  Proof.
    destruct e as (f, H).
    destruct H.
    reflexivity.
  Qed.

  Lemma seqv_to_eqv_to_seqv A B (e : A <~> B) : e = seqv_to_eqv (eqv_to_seqv e).
  Proof.
    destruct e as (f, H).
    unfold seqv_to_eqv, eqv_to_seqv.
    apply ap.
    refine (path_ishprop _ _).
    refine (hprop_isequiv _).
    apply Funext_type_implies_Funext.
    exact funext.
  Qed.

  Lemma eqv_ind A (P : forall B, A <~> B -> Type) B e B' e' :
    P B e -> P B' e'.
  Proof.
    intro p.
    rewrite seqv_to_eqv_to_seqv.
    pose (P' := fun B e => P B (seqv_to_eqv e)).
    apply (seqv_ind' _ P' B (eqv_to_seqv e) B' (eqv_to_seqv e')).
    unfold P'.
    rewrite <- seqv_to_eqv_to_seqv.
    exact p.
  Defined.

  Lemma eqv_ind_id {A P} B e : eqv_ind A P B e B e = idmap.
  Proof.
    unfold eqv_ind.
    rewrite seqv_ind_id.
    generalize (seqv_to_eqv_to_seqv A B e).
    intro H.
    destruct H.
    reflexivity.
  Qed.

  Definition eqv_induction' {A} (P : forall B, A <~> B -> Type) B e :
    P A (equiv_idmap A) -> P B e :=
    eqv_ind A P A (equiv_idmap A) B e.

  Definition eqv_induction_beta' {A} (P : forall B, A <~> B -> Type) :
    eqv_induction' P A (equiv_idmap A) = idmap :=
    eqv_ind_id A (equiv_idmap A).

End from_eqvtoseqv_to_eqv_induction.

Section from_eqv_induction_to_eqv_equivalence.

  Variable eqv_induction : forall A (P : forall B, A <~> B -> Type) B e,
    P A (equiv_idmap A) -> P B e.

  Hypothesis eqv_induction_beta : forall A (P : forall B, A <~> B -> Type),
      eqv_induction A P A (equiv_idmap A) = idmap.

  Theorem eqv_eqv_seqv {A B} (f : A -> B) : (IsEquiv f) <~> (isStrongEqv f).
  Proof.
    exists (iseqv_to_seqv eqv_induction f).
    apply (isequiv_adjointify _ is_seqv_to_eqv).
    - intros [].
      unfold iseqv_to_seqv.
      rewrite eqv_induction_beta.
      reflexivity.
    - intro Hf.
      refine (path_ishprop _ _).
      refine (hprop_isequiv _).
      apply Funext_type_implies_Funext.
      exact (funext (@iseqv_to_seqv eqv_induction)).
  Qed.
End from_eqv_induction_to_eqv_equivalence.

Section from_eqv_equivalence_to_univalence.
  Polymorphic Hypothesis iseqv_eqv_isseqv : forall A B (f : A -> B), (IsEquiv f) <~> (isStrongEqv f).
  Hypothesis iseqv_eqv_isseqv' : forall A B (f : A -> B), (IsEquiv f) <~> (isStrongEqv f).
  Hypothesis iseqv_eqv_isseqv'' : forall A B (f : A -> B), (IsEquiv f) <~> (isStrongEqv f).

  Lemma eqv_to_seqv' {A B} : A <~> B -> A ≃ B.
  Proof.
    intros (f, e).
    exists f.
    apply iseqv_eqv_isseqv.
    exact e.
  Defined.

  Lemma eqv_to_seqv'' {A B} : A <~> B -> A ≃ B.
  Proof.
    intros (f, e).
    exists f.
    apply iseqv_eqv_isseqv'.
    exact e.
  Defined.

  Lemma seqv_to_eqv' {A B} : A ≃ B -> A <~> B.
  Proof.
    intros (f, e).
    exists f.
    apply (symmetric_equiv _ _ (iseqv_eqv_isseqv A B f)).
    exact e.
  Defined.

  Lemma isProp_IsEquiv' {A B} {f : A -> B} : IsHProp (IsEquiv f).
  Proof.
    refine (hprop_isequiv _).
    apply Funext_type_implies_Funext.
    exact (funext (@iseqv_eqv_isseqv)).
  Qed.

  (* This is true in general but I did not find it in the library so I
  reproved it here using the available assumptions to simplify the
  proof. *)
  Lemma IsHProp_Equiv {A B} : IsHProp A -> A <~> B -> IsHProp B.
  Proof.
    intros HA He.
    apply eqv_to_seqv'' in He.
    destruct He as (f, e).
    destruct e.
    assumption.
  Qed.

  Lemma isProp_isStrongEqv {A B} (f : A -> B) : IsHProp (isStrongEqv f).
  Proof.
    refine (IsHProp_Equiv _ (iseqv_eqv_isseqv'' _ _ f)).
    apply isProp_IsEquiv'.
  Qed.

  Lemma seqv_eqv_eqv' {A B} : (A ≃ B) <~> (A <~> B).
  Proof.
    exists seqv_to_eqv'.
    apply (isequiv_adjointify _ eqv_to_seqv');
      unfold seqv_to_eqv', eqv_to_seqv'.
    - intros (f, Hf).
      simpl.
      apply ap.
      apply isProp_IsEquiv'.
    - intros (f, Hf).
      simpl.
      apply ap.
      refine (path_ishprop _ _).
      exact (isProp_isStrongEqv f).
  Defined.

  Lemma ua_inv {A B} : (A = B) <~> (A <~> B).
  Proof.
    apply (transitive_equiv
             (A = B) (seqv A B) (A <~> B)
             (symmetric_equiv (seqv A B) (A = B) seqv_eqv_eq)
             seqv_eqv_eqv').
  Defined.

  Lemma ua_inv_refl A : ua_inv (idpath A) = equiv_idmap A.
  Proof.
    unfold ua_inv, transitive_equiv, equiv_idmap.
    simpl.
    unfold seqv_to_eqv'.
    apply ap.
    apply isProp_IsEquiv'.
  Qed.

  Lemma univalence {A B} : IsEquiv (equiv_path A B).
  Proof.
    apply (isequiv_homotopic ua_inv).
    intros [].
    apply ua_inv_refl.
  Qed.

End from_eqv_equivalence_to_univalence.

(*

Inductive heq {A : Type} (a : A) : forall {B : Type}, (A -> B) -> B -> Type :=
| refl : heq a idmap a.

Definition heq2 {A B} (f : A -> B) (a : A) (b : B) :=
  ((f a = b) * isStrongEqv f)%type.

Definition hom_eq {A : Type} (a b : A) := heq a idmap b.

Lemma heq_heq2 {A B : Type} {f : A -> B} {a b} : heq a f b -> heq2 f a b.
Proof.
  intro h.
  destruct h.
  split.
  - reflexivity.
  - apply isStrongEqv_id.
Defined.

Lemma heq2_heq {A B : Type} {f : A -> B} {a b} : heq2 f a b -> heq a f b.
Proof.
  intros (e, Hf).
  destruct Hf.
  destruct e.
  apply refl.
Defined.

Lemma eqv_heq_heq2 {A B} {f : A -> B} {a b} : heq a f b <~> heq2 f a b.
Proof.
  exists heq_heq2.
  apply (isequiv_adjointify _ heq2_heq).
  - intros (e, Hf).
    destruct Hf.
    destruct e.
    reflexivity.
  - intros [].
    reflexivity.
Qed.


Section corollaries.

  Hypothesis iseqv_eqv_isseqv : forall A B (f : A -> B), IsEquiv f <~> isStrongEqv f.

  Hypothesis IsHProp_isStrongEqv : forall A B (f : A -> B), IsHProp (isStrongEqv f).

  Hypothesis funext :
    forall (A B : Type) (f g : A -> B), (forall x : A, f x = g x) -> f = g.

  Lemma heq_eqv_eq {A B} (f : A ≃ B) a b : heq a f b <~> (f a = b).
  Proof.
    transitivity (heq2 f a b); [exact eqv_heq_heq2|].
    apply (transitive_equiv _ _ _ eqv_heq_heq2).
    apply (@transitive_equiv _ _ _ _ ((f a = b) * unit)%type).
    - unfold heq2.
      apply eqv_prod.
      + apply equiv_idmap.
      + apply Contr_unit.
        apply IsHProp_to_Contr.
        * destruct f.
          assumption.
        * apply IsHProp_isStrongEqv.
    - apply eqv_prod_unit.
  Qed.

  Corollary heq_seqv_eq {A B} (f : A ≃ B) a b : heq a f b ≃ (f a = b).
  Proof.
    apply (eqv_to_seqv' iseqv_eqv_isseqv).
    apply heq_eqv_eq.
  Qed.

  Definition heq_eq_eq {A B} (f : A ≃ B) a b : heq a f b = (f a = b) :=
    seqv_to_eq (heq_seqv_eq f a b).

  Corollary heq_isseqv_eq {A B} (f : A -> B) {a b} : isStrongEqv f -> heq a f b ≃ (f a = b).
  Proof.
    intro H.
    pose (g := existT isStrongEqv f H : A ≃ B).
    change (seqv (heq a g b) (g a = b)).
    apply heq_seqv_eq.
  Qed.

  Lemma hom_eq_seqv_eq (A : Type) (a b : A) : hom_eq a b ≃ (a = b).
  Proof.
    change (heq a (id A) b ≃ (id A a = b)).
    apply heq_isseqv_eq.
    constructor.
  Defined.

  Lemma hom_eq_eq_eq (A : Type) (a b : A) : hom_eq a b = (a = b).
  Proof.
    apply seqv_to_eq.
    apply hom_eq_seqv_eq.
  Defined.

  Lemma hom_eq_eq_eq_fun (A : Type) : (@hom_eq A) = eq.
  Proof.
    apply funext; intro a.
    apply funext; intro b.
    apply hom_eq_eq_eq.
  Defined.

  Lemma hom_eq_eq_eq_fun_rev (A : Type) : eq = (@hom_eq A).
  Proof.
    apply eq_sym.
    apply hom_eq_eq_eq_fun.
  Defined.

  (* This is similar to has_eqind but not restricted to type equality
     and it also contains the computational rule. *)
  Definition has_full_eqind (A : Type) (E : A -> A -> Type) :=
    sigma (ind : forall a (P : forall b : A, E a b -> Type) b e b' e', P b e -> P b' e'), forall a P b e, hom_eq (ind a P b e b e) (id _).

  Lemma eq_ind A a (P : forall b : A, a = b -> Type) b e b' e' :
    P b e -> P b' e'.
  Proof.
    destruct e.
    destruct e'.
    apply id.
  Defined.

  Lemma has_full_eqind_eq A : has_full_eqind A eq.
  Proof.
    exists (eq_ind A).
    intros a P b e.
    destruct e.
    simpl.
    apply refl.
  Defined.

  Lemma has_full_eqind_hom_eq A : has_full_eqind A hom_eq.
  Proof.
    apply (transport (has_full_eqind A) (hom_eq_eq_eq_fun_rev A)).
    apply has_full_eqind_eq.
  Defined.

  Lemma hom_eq_ind A (a : A) (P : forall b : A, hom_eq a b -> Type) b e b' e' :
      P b e -> P b' e'.
  Proof.
    apply has_full_eqind_hom_eq.
  Defined.

  Lemma hom_eq_induction (A : Type) (a : A) (P : forall b : A, hom_eq a b -> Type) b e :
    P a (refl a) -> P b e.
  Proof.
    apply hom_eq_ind.
  Defined.

  Eval compute in (fun A a P => hom_eq_induction A a P a (refl a)).

  Lemma hom_eq_induction_heq (A : Type) (a : A) (P : forall b : A, hom_eq a b -> Type) :
    hom_eq (hom_eq_induction A a P a (refl a)) (id _).
  Proof.
    unfold hom_eq_induction, hom_eq_ind.
    destruct has_full_eqind_hom_eq as (ind, H).
    apply H.
  Defined.

End corollaries.

*)

(* Local Variables: *)
(* coq-prog-name: "~/.opam/4.07.1/bin/hoqtop" *)
(* coq-load-path: nil *)
(* End: *)
